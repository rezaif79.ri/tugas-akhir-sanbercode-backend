from models.models import database
from controllers import controller
from flask import Flask, jsonify, request
from flask_jwt_extended import JWTManager
import json

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.config['JWT_SECRET_KEY'] = 'kuncirahasia'

mysqldb = database()
jwt = JWTManager(app)


@app.route("/")
def main():
    return "Selamat datang di KASIR_TOKO API !"

@app.route("/requesttoken", methods=["POST"])
def requestToken():
    params = request.json
    return controller.token(**params)

# ================ ROUTE UNTUK KASIR ================ #
@app.route("/kasir", methods=["POST"])
def kasir():
    return controller.showKasir()

@app.route("/kasir/find", methods=["POST"])
def kasirById():
    params = request.json
    return controller.showKasirById(**params)

@app.route("/kasir/insert", methods=["POST"])
def insertKasir():
    params = request.json
    return controller.insertKasir(**params)

@app.route("/kasir/update", methods=["POST"])
def updateKasirById():
    params = request.json
    return controller.updateKasirById(**params)

@app.route("/kasir/delete", methods=["POST"])
def deleteKasirById():
    params = request.json
    return controller.deleteKasirById(**params)

# ================ ROUTE UNTUK PRODUK ================ #
@app.route("/produk", methods=["POST"])
def produk():
    return controller.showProduk()

@app.route("/produk/find", methods=["POST"])
def produkById():
    params = request.json
    return controller.showProdukById(**params)

@app.route("/produk/insert", methods=["POST"])
def insertProduk():
    params = request.json
    return controller.insertProduk(**params)

@app.route("/produk/update", methods=["POST"])
def updateProdukById():
    params = request.json
    return controller.updateProdukById(**params)

@app.route("/produk/delete", methods=["POST"])
def deleteProdukById():
    params = request.json
    return controller.deleteProdukById(**params)

# ================ ROUTE UNTUK PRODUK ================ #
@app.route("/transaksi", methods=["POST"])
def transaksi():
    return controller.showTransaksi()

@app.route("/transaksi/find", methods=["POST"])
def transaksiById():
    params = request.json
    return controller.showTransaksiById(**params)

@app.route("/transaksi/insert", methods=["POST"])
def insertTransaksi():
    params = request.json
    return controller.insertTransaksi(**params)

@app.route("/transaksi/update", methods=["POST"])
def updateTransaksiById():
    params = request.json
    return controller.updateTransaksiById(**params)

@app.route("/transaksi/delete", methods=["POST"])
def deleteTransaksiById():
    params = request.json
    return controller.deleteTransaksiById(**params)

if __name__ == "__main__":
    mysqldb = database()
    if mysqldb.db.is_connected():
        print('Connected to MySQL database')
    
    app.run(debug=True)
    
    if mysqldb.db is not None and mysqldb.db.is_connected():
        mysqldb.db.close()