from flask_jwt_extended import *
from models.models import database
from flask import Flask, jsonify, request
from datetime import datetime

mysqldb = database()

# ========================== Controller untuk request auth key =============================== #
def token(**params):
    dbresult = mysqldb.showKasirById(**params)
    if dbresult is not None:
        kasir = {
            "id_kasir" : dbresult[0],
            "nama_kasir" : dbresult[1]            
        }
        expires = datetime.timedelta(days=1)
        expires_refresh = datetime.timedelta(days=3)
        access_token = create_access_token(kasir, fresh=True, expires_delta=expires)
        
        data = {
            "data": kasir,
            "token_access": access_token
        }

    else:
        data = {
            "message":"Data kasir tidak terdaftar"
        }
        
    return jsonify(data)


# ========================== Controller untuk Kasir =============================== #
@jwt_required()
def showKasir():
    dbresult = mysqldb.showKasir()
    result = []
    print(dbresult)
    for items in dbresult:
        kasir = {
            "id_kasir" : items[0],
            "nama_kasir" : items[1],            
        }
        result.append(kasir)
        
    return jsonify(result)

@jwt_required()
def showKasirById(**params):
    dbresult = mysqldb.showKasirById(**params)
    print(type(dbresult) == str)
    if not(type(dbresult) is (tuple or dict)):
        return str(Exception("Tidak ditemukan"))
    else:
        kasir = {
            "id_kasir" : dbresult[0],
            "nama_kasir" : dbresult[1],            
        }
        return jsonify(kasir)

@jwt_required()
def insertKasir(**params):
    status = mysqldb.insertKasir(**params)
    mysqldb.db.commit()
    return status   

@jwt_required()
def updateKasirById(**params):
    status = mysqldb.updateKasirById(**params)
    return status

@jwt_required()
def deleteKasirById(**params):
    status = mysqldb.deleteKasirById(**params)
    return status 

# ========================== Controller untuk Produk =============================== #
@jwt_required()
def showProduk():
    dbresult = mysqldb.showProduk()
    result = []
    print(dbresult)
    for items in dbresult:
        produk = {
            "kode_produk" : items[0],
            "nama_produk" : items[1],
            "harga_produk" : items[2],            
        }
        result.append(produk)
        
    return jsonify(result)

@jwt_required()
def showProdukById(**params):
    dbresult = mysqldb.showProdukById(**params)
    if not(type(dbresult) is (tuple or dict)):
        return str(Exception("Tidak ditemukan"))
    else:
        print(dbresult)
        produk = {
                "kode_produk" : dbresult[0],
                "nama_produk" : dbresult[1],
                "harga_produk" : dbresult[2],            
            }
        return jsonify(produk)
        

@jwt_required()
def insertProduk(**params):
    id = str(params["values"]["kode_produk"])
    id = id.zfill(5)
    params["values"]["kode_produk"] = "PS" + id
    status = mysqldb.insertProduk(**params)
    mysqldb.dataCommit()

    return (status)

@jwt_required()
def updateProdukById(**params):
    status = mysqldb.updateProdukById(**params)
    return status

@jwt_required()
def deleteProdukById(**params):
    status = mysqldb.deleteProdukById(**params)
    return status 

# ========================== Controller untuk Transaksi/Pembelian =============================== #
@jwt_required()
def showTransaksi():
    dbresult = mysqldb.showTransaksi()
    result = []
    print(dbresult)
    for items in dbresult:
        transaksi = {
            "kode_ref" : items[0],  
            "kode_produk" : items[1],
            "id_kasir" : items[2],
            "jumlah" : items[3], 
            "tanggal" : items[3],            
        }
        result.append(transaksi)
        
    return jsonify(result)

@jwt_required()
def showTransaksiById(**params):
    dbresult = mysqldb.showTransaksiById(**params)
    if not(type(dbresult) is (tuple or dict)):
        return str(Exception("Tidak ditemukan"))
    else :
        transaksi = {
                "kode_ref" : dbresult[0],  
                "kode_produk" : dbresult[1],
                "id_kasir" : dbresult[2],
                "jumlah" : dbresult[3], 
                "tanggal" : dbresult[3],            
            }
        return jsonify(transaksi)

@jwt_required()
def insertTransaksi(**params):
    result = mysqldb.showTransaksi()
    count = len(result) + 1
    id = str(count)
    params["values"]["kode_ref"] = "REF" + id.zfill(5)
    params["id_kasir"] = "1"
    params["tanggal"] = "NOW()"
    
    status = mysqldb.insertTransaksi(**params)
    mysqldb.dataCommit()

    return status

@jwt_required()
def updateTransaksiById(**params):
    status = mysqldb.updateTransaksiById(**params)
    return status 

@jwt_required()
def deleteTransaksiById(**params):
    status = mysqldb.deleteTransaksiById(**params)
    return status 