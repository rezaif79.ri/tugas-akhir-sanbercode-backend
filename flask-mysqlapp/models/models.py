from mysql.connector import connect
import json


# Class ini digunakan sebagai koneksi kepada database 'kasir_toko' yang berisi 3 tabel 
# (tb_kasir, tb_produk, tb_pembelian) dapat dilihat di ERD 

class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='kasir_toko',
                              user='root',
                              password='root123')
        except Exception as e:
            print(e)

    def dataCommit(self):
        self.db.commit()
    
    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(item[0],item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result

# ========================== Function untuk tb_kasir =============================== #
# Function untuk tabel tb_kasir yang berisi nomor id kasir dan nama kasir nya
    def showKasir(self):
        try:
            cursor = self.db.cursor()
            query ='''select * from tb_kasir'''
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except Exception as e:
            return str(e)

    def showKasirById(self, **params):
        try:
            
            cursor = self.db.cursor()
            query = '''
                select * 
                from tb_kasir 
                where id_kasir = {0};
            '''.format(params["id_kasir"])
            
            cursor.execute(query)
            
            result = cursor.fetchone()
            if(result == None):
                raise Exception("Tidak ditemukan")
            return result
        except Exception as e:
            return str(e)

    def insertKasir(self, **params):
        try:
            column = ', '.join(list(params['values'].keys()))
            values = tuple(list(params['values'].values()))
            crud_query = '''insert into tb_kasir ({0}) values {1};'''.format(column, values)
            
            cursor = self.db.cursor()
            cursor.execute(crud_query)
            self.db.commit()
            return("(tb_kasir)Insert Success")
        except Exception as e:
            return str(e)

    def updateKasirById(self, **params):
        try:
            id_kasir = params["values"]['id_kasir']
            values = self.restructureParams(**params['values'])
            crud_query = '''update tb_kasir set {0} where id_kasir = {1};'''.format(values, id_kasir)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
            self.db.commit()
            return ("(tb_kasir) Update success")
        except Exception as e:
            return str(e)

    def deleteKasirById(self, **params):
        try:
            id_kasir = params['id_kasir']
            crud_query = '''delete from tb_kasir where id_kasir = {0};'''.format(id_kasir)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
            self.db.commit()
            return("(tb_kasir) Delete success")
        except Exception as e:
            return str(e)

# ========================== Function untuk tb_produk =============================== #
# Function untuk tabel tb_produk yang berisi kode_produk, nama_produk dan harga
    def showProduk(self):
        try:
            cursor = self.db.cursor()
            query ='''select * from tb_produk'''
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except Exception as e:
            return str(e)

    def showProdukById(self, **params):
        try:
            cursor = self.db.cursor()
            query = '''
                select * 
                from tb_produk 
                where kode_produk = "{0}";
            '''.format(params["kode_produk"])
            
            cursor.execute(query)
            result = cursor.fetchone()
            
            if(result == None):
                raise Exception("Tidak ditemukan")
            return result
        except Exception as e:
            return str(e)

    def insertProduk(self, **params):
        try:
            column = ', '.join(list(params['values'].keys()))
            values = tuple(list(params['values'].values()))
            crud_query = '''insert into tb_produk ({0}) values {1};'''.format(column, values)
            
            cursor = self.db.cursor()
            cursor.execute(crud_query)
            respon = "(tb_produk)Insert Success"
            return respon
        except Exception as e:
            return str(e)

    def updateProdukById(self, **params):
        try:
            kode_produk = params["values"]['kode_produk']
            values = self.restructureParams(**params['values'])
            crud_query = '''update tb_produk set {0} where kode_produk = {1};'''.format(values, kode_produk)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
            self.db.commit()
            return ("(tb_produk) Update success")
        except Exception as e:
            return str(e)

    def deleteProdukById(self, **params):
        try:
            kode_produk = params['kode_produk']
            crud_query = '''delete from tb_produk where kode_produk = {0};'''.format(kode_produk)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
            self.db.commit()
            return("(tb_produk) Delete success")
        except Exception as e:
            return str(e)

# ========================== Function untuk tb_pembelian =============================== #
# Function untuk tabel tb_pembbelian yang berisi kode_ref, kode_produk, id_kasir, jumlah dan tanggal
    def showTransaksi(self):
        try:
            cursor = self.db.cursor()
            query ='''select * from tb_pembelian'''
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except Exception as e:
            return str(e)

    def showTransaksiById(self, **params):
        try:
            cursor = self.db.cursor()
            query = '''
                select * 
                from tb_pembelian 
                where kode_ref = "{0}";
            '''.format(params["kode_ref"])
            
            cursor.execute(query)
            result = cursor.fetchone()
            return result
        except Exception as e:
            return str(e)

    def insertTransaksi(self, **params):
        try:
            column = ', '.join(list(params['values'].keys()))
            values = tuple(list(params['values'].values()))
            crud_query = '''insert into tb_pembelian ({0}) values {1};'''.format(column, values)
            
            cursor = self.db.cursor()
            cursor.execute(crud_query)
            return("(tb_pembelian)Insert Success")
        except Exception as e:
            return str(e)

    def updateTransaksiById(self, **params):
        try:
            kode_ref = params["values"]['kode_ref']
            values = self.restructureParams(**params['values'])
            crud_query = '''update tb_pembelian set {0} where kode_ref = {1};'''.format(values, kode_ref)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
            self.db.commit()
            return ("(tb_pembelian) Update success")
        except Exception as e:
            return str(e)

    def deleteTransaksiById(self, **params):
        try:
            kode_ref = params['kode_ref']
            crud_query = '''delete from tb_pembelian where kode_ref = {0};'''.format(kode_ref)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
            self.db.commit()
            return("(tb_pembelian) Delete success")
        except Exception as e:
            return str(e)
