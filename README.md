# Tugas Akhir Sanbercode Backend
Oleh Reza Izzan Fadhila

====================================================================

Membuat backend dari sebuah aplikasi sederhana. dengan menerapkan konsep microservices.  
Dalam Project ini, Basis data yang digunakan adalah MySQL dan Menggunakan Framework Flask
Dengan authentification jwt

====================================================================

**Tentang Project Ini**

Project ini adalah tugas akhir dari kelas intensif Sanbercode Backend microservices dengan Python.
*jangan lupa import database nya (kasir_toko.sql) agar bisa digunakan

**Database**

Aplikasi API pada Project ini digunakan untuk mencatat data kasir, produk, dan transaksi/pembelian dari suatu toko.
Didalam nya terdapat 1 database MySQL (nama: kasir_toko) dengan 3 tabel (tb_kasir, tb_produk dan tb_pembelian)

tb_kasir berisi id_kasir dan nama_kasir dengan id_kasir sebagai primary key dan bertipe data int
Saya tidak menerapkan auto increment karena berdasarkan pengalaman saya,
id yang sudah terhapus tidak mengulang kembali ke angka sebelumnya

tb_produk berisi kode_produk, nama_produk, dan harga dengan kode_produk sebagai primary key dan bertipe data varchar
kolom kode_produk di input dengan kombinasi string dan zero fill, contoh nya kita input kode_produk nya 1 akan
ter input kode PS00001 dengan kombinasi str "PS" dan zerofill 5 angka.

dan tb_pembelian berisi kode_ref, kode_produk, id_kasir, jumlah dan tanggal
kode_ref sebagai primary key dengan tipe data varchar, berisi kombinasi string dan zerofill
contoh nya kita input kode_produk nya 1 akan ter input kode REF00001 dengan kombinasi str "REF" dan zerofill 5 angka.
kode_produk adalah foreign key dari tb_produk, id_kasir adalah foreign key dari id_kasir
jumlah adalah int yang mencatat kuantitas barang yang dibeli
tanggal adalah datetime yang mencatat tanggal dan waktu transaksi

**Cara Penggunaan API**

Pertama, Untuk menggunakan API ini, Harus mendapatkan kode autentifikasi pada /requesttoken menggunakan metode post
Contoh:
{
    "id_kasir" : 1,
    "nama_kasir" : "Amalia"
}

Lalu pada setiap Pemanggilan API,isi bagian pada tab auth, gunakan bearer key dari kode yang didapat diatas

**Route API**

/requesttoken = Untuk mendapatkan token auth yang digunakan untuk Pemanggilan API, Digunakan pada tab auth di bearer key 
/kasir = melihat data kasir yang ada
/kasir/find = melihat data kasir dengan id_kasir, gunakan parameter:

```
{
    "id_kasir" : id nya
}
```

/kasir/insert = untuk menambahkan data kasir baru, gunakan parameter:

```
{
    "values":{
        "id_kasir": (angka id),
        "nama_kasir" : "Kasir baru"
    }
}
```

/kasir/update = untuk mengubah data kasir yang sudah ada, gunakan parameter:

```
{
    "values":{
        "id_kasir": (angka id),
        "nama_kasir" : "Kasir baru"
    }
}
```

/kasir/delete = untuk menghapus data kasir yang ada menggunakan id kasir, gunakan parameter :

```
{
    "id_kasir" : id nya
}
```

/produk = Melihat data produk yang ada
/produk/find = melihat data produk dengan input kode_produk, gunakan parameter:

```
{
    "kode_produk": "PS00001", #contoh
}
```

/produk/insert = untuk menambahkan data produk baru, gunakan parameter (contoh) :

```
{
    "values":{"kode_produk": 3,
    "nama_produk" : "Pensil Carbon",
    "harga_produk" : 8000}
}
```

/produk/update = untuk mengubah data produk yang sudah ada, gunakan parameter:

```
{
    "values":{
        "kode_produk": (angka id),
        "harga_produk" : (harga baru)
    }
}
```

/produk/delete = menghapus data produk dengan input kode_produk, gunakan parameter:

```
{
    "kode_produk": "PS00001", #contoh
}
```

/transaksi = untuk melihat data pembelian yang terjadi
/transaksi/find = untuk melihat data transaksi dengan kode_ref, gunakan parameter :

```
{
    "kode_ref" : "REF00001" #Contoh
}
```

/transaksi/insert = untuk menginput data transaksi baru, gunakan parameter (contoh):

```
{
    "values":{
        "kode_produk": "PS00001",
        "id_kasir" : 1,
        "jumlah" : 2
        }
}
```

/transaksi/update = untuk mengubah data transaksi yang sudah ada, gunakan parameter:

```
{
    "values":{
        "kode_ref": (angka id),
        "jumlah" : (jumlah baru)
    }
}
```

/transaksi/delete = menghapus data transaksi dengan input kode_ref, gunakan parameter:

```
{
    "kode_ref": "REF00001", #contoh
}
```





